const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const config = require('config');

const port = 8080;

let app = express();

mongoose.connect(config.DBHost)
    .then(function () {
        console.log('Mongoose default connection open to ' + config.DBHost);
    })
    .catch(function (err) {
        throw err;
    });

if(config.util.getEnv('NODE_ENV') !== 'test') {
    app.use(morgan('combined'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));

let router = require('./app/routers/restaurants.router');

app.use('/api', router);

app.listen(port, function(err){
    if(err) throw err;
    console.log("App listening on port " + port);
});

module.exports = app;