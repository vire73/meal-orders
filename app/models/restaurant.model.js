const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MealSchema = Schema({
    name: {
        type: String,
        required: [true, "required"]
    },
    quantity: {
        type: Number,
        min: 0,
        required: [true, "required"]
    }
});

let RestaurantSchema = Schema({
    name: {
        type: String,
        unique: true,
        required: [true, "required"]
    },
    rating: {
        type: Number,
        min: 0,
        max: 5,
        required: [true, "required"]
    },
    meals: [MealSchema]
});

let RestaurantModel = mongoose.model('Restaurant', RestaurantSchema);

module.exports = RestaurantModel;