const Restaurant = require('../models/restaurant.model');

//TODO Consider moving the meal ordering stuff into its own routes/controllers/etc.
const RestaurantCtrl = {
    GetRestaurant: function (req, res) {
        Restaurant.find({}).exec()
            .then(function (restaurants) {
                res.json(restaurants);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    PostRestaurant: function (req, res) {
        let restaurant = new Restaurant(req.body);
        restaurant.save()
            .then(function (restaurant) {
                res.json(restaurant);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    UpdateRestaurant: function (req, res) {
        let data = req.body;
        Restaurant.findByIdAndUpdate(req.params.id, {$set: data}).exec()
            .then(function (restaurant) {
                res.json(restaurant);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    },
    DeleteRestaurant: function (req, res) {
        //TODO add delete restaurant function
    },
    GetMealList: function (req, res) {
        let mealRequest = req.body;

        if(!validateMealRequest(mealRequest)){
            throw new Error("Invalid Request");
        }

        normalizeMealRequestObject(mealRequest);

        let mealResponse = {total: {}};

        Restaurant.find({}).sort('-rating').exec()
            .then(function (restaurants) {
                let totalFoundMeals = 0;

                //TODO clean this up
                mealRequest.meals.forEach(function (mealType) {
                    if(!mealResponse.total[mealType.name]){
                        mealResponse.total[mealType.name] = 0;
                    }

                    restaurants.forEach(function(restaurant){
                        if(!mealResponse[restaurant.name]){
                            mealResponse[restaurant.name] = {};
                        }

                        if(restaurant.meals.find(meal => meal.name === mealType.name) && mealResponse.total[mealType.name] < mealType.quantity){
                            let requestCount = mealRequest.meals.find(meal => meal.name === mealType.name).quantity;
                            let availableCount = restaurant.meals.find(meal => meal.name === mealType.name).quantity;

                            let totalNeeded = requestCount - mealResponse.total[mealType.name];
                            let totalDiff = availableCount - totalNeeded;

                            let foundMeals = totalDiff >= 0 ? totalNeeded : availableCount;

                            mealResponse[restaurant.name][mealType.name] = foundMeals;
                            mealResponse.total[mealType.name] += foundMeals;
                            totalFoundMeals += foundMeals;
                        }
                    });
                });

                let isFulfilled = true;
                mealRequest.meals.forEach(function (mealType) {
                    if (!mealResponse.total[mealType.name] || mealResponse.total[mealType.name] !== mealType.quantity) {
                        isFulfilled = false;
                    }
                });

                mealResponse.status = isFulfilled ? "Order fulfilled" : "Unable to fulfill order";

                res.json(mealResponse);
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    }
};

function validateMealRequest(mealRequest){
    //TODO Make the request validation more thorough
    if(!mealRequest.total || !mealRequest.meals) {
        return false;
    }
    return true;
}

function normalizeMealRequestObject(mealRequest) {
    if(!mealRequest.meals.find(meal => meal.name === 'normal')) {
        let normalQuantity = mealRequest.total;
        mealRequest.meals.forEach(function (mealType) {
            normalQuantity -= mealType.quantity;
        });
        mealRequest.meals.push({
            name: "normal",
            quantity: normalQuantity
        });
    }
}

module.exports = RestaurantCtrl;