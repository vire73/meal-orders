const express = require('express');
const restaurantsRouter = express.Router();

const RestaurantController = require('../controllers/restaurants.controller');

//Basic Restaurant CRUD
restaurantsRouter.get('/restaurant', RestaurantController.GetRestaurant);

restaurantsRouter.post('/restaurant', RestaurantController.PostRestaurant);

restaurantsRouter.delete('/restaurant/:id', RestaurantController.DeleteRestaurant);

restaurantsRouter.put('/restaurant/:id', RestaurantController.UpdateRestaurant);

//Meal ordering functionality
restaurantsRouter.post('/get-meal-list', RestaurantController.GetMealList);

module.exports = restaurantsRouter;