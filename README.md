# meal-orders

This project was built using node v10.5.0, npm 6.1.0, and mongodb v3.6.5.

Install by using 'npm install' in the meal-orders dir.

To start the server, make sure mongod is started and use 'npm start'.

To run test suite, make sure mongod is started and use 'npm test'.

---------------------------------
API

Right now there is a single entity: Restaurant. Here is an example document:
{
    name: "Restaurant A",
    rating: 5,
    meals: [
        {
            name: "normal",
        	quantity: 36
        },
        {
            name: "vegetarian",
        	quantity: 4
    	}
	]
}

A restaurant and its meals can be manipulated with the following API calls:

GET /api/restaurant
-returns all restaurants

POST /api/restaurant
-creates a new restaurant from a provided json object

PUT /api/restaurant/:id
-updates an existing restaurant with provided json data

The meal ordering functionality can be accessed through the following call:

POST /api/get-meal-list

It expectes a meal request json object. Here is an example object:
{
	total: 50,
	meals: [
	{
		name: "vegetarian",
		quantity: 5
	},
 	{
 		name: "gluten-free",
 		quantity: 7
	}
	]
}

It will return an object containing the order info and a status:
{
	total: {
		vegetarian: 5,
		'gluten-free': 7,
		normal: 38
	},
	'Restaurant A': {
		vegetarian: 4,
		normal: 36
	},
	'Restaurant B': {
		vegetarian: 1,
		'gluten-free': 7,
		normal: 2
	},
	status: 'Order fulfilled'
}