process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');
const Restaurant = require("../app/models/restaurant.model");


const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');

const server = require('../server');

chai.use(chaiHttp);

describe('Restaurants', function () {
    beforeEach(function () {
        return Restaurant.remove({})
            .then(function () {
            })
            .catch(function (err) {
                throw err;
            });
    });

    describe("/GET restaurant", function () {
        it("should return all restaurants", function () {
            return chai.request(server).get('/api/restaurant')
                .then(function (res) {
                    res.should.have.status(200);
                    res.body.should.be.an('array');
                    res.body.length.should.equal(0);
                })
                .catch(function (err) {
                    throw err;
                });
        });

        it("should return a restaurant specified by the id");
    });

    describe("/POST restaurant", function () {
        it("should create new restaurant", function () {
            let restaurant = {
                name: "Restaurant A",
                rating: 5,
                meals: [
                    {
                        name: "normal",
                        quantity: 38
                    },
                    {
                        name: "vegetarian",
                        quantity: 5
                    },
                    {
                        name: "gluten-free",
                        quantity: 7
                    }
                ]
            };

            return chai.request(server).post('/api/restaurant').send(restaurant)
                .then(function (res) {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.name.should.equal(restaurant.name);
                })
                .catch(function (err) {
                    throw err;
                });
        });
        it("should not create restaurant missing properties", function () {
            let restaurant = {
                rating: 5,
                meals: [
                    {
                        name: "normal",
                        quantity: 38
                    },
                    {
                        name: "vegetarian",
                        quantity: 5
                    },
                    {
                        name: "gluten-free",
                        quantity: 7
                    }
                ]
            };

            return chai.request(server).post('/api/restaurant').send(restaurant)
                .then(function (res) {
                    res.should.have.status(400);
                    res.body.should.be.an('object');
                    res.body.should.have.property('errors');
                    res.body.should.have.property('name');
                    res.body.should.have.property('message');
                    res.body.message.should.equal('Restaurant validation failed: name: required');
                })
                .catch(function (err) {
                    throw err;
                });
        });
    });

    describe("Get Meal List", function () {
        let restA = new Restaurant({
            name: "Restaurant A",
            rating: 5,
            meals: [
                {
                    name: "normal",
                    quantity: 36
                },
                {
                    name: "vegetarian",
                    quantity: 4
                }
            ]
        });
        let restB = new Restaurant({
            name: "Restaurant B",
            rating: 3,
            meals: [
                {
                    name: "normal",
                    quantity: 60
                },
                {
                    name: "vegetarian",
                    quantity: 20
                },
                {
                    name: "gluten-free",
                    quantity: 20
                }
            ]
        });

        beforeEach(function () {
            return restA.save()
                .then(function () {
                    return restB.save();
                })
                .catch(function (err) {
                    throw err;
                });
        });

        it("should return a correct meal order", function () {
            let mealRequest = {
                total: 50,
                meals: [
                    {
                        name: "vegetarian",
                        quantity: 5
                    },
                    {
                        name: "gluten-free",
                        quantity: 7
                    }
                ]
            };

            let expectedResponse = {
                total: {
                    vegetarian: 5,
                    'gluten-free': 7,
                    normal: 38
                },
                'Restaurant A': {
                    vegetarian: 4,
                    normal: 36
                },
                'Restaurant B': {
                    vegetarian: 1,
                    'gluten-free': 7,
                    normal: 2
                },
                status: 'Order fulfilled'
            };

            return chai.request(server).post('/api/get-meal-list').send(mealRequest)
                .then(function (res) {
                    res.should.have.status(200);
                    res.body.should.deep.equal(expectedResponse);
                })
                .catch(function (err) {
                    throw err;
                });
        });

        it("should return an error if request is invalid");
    });
});